<?php


namespace app\models;

use app\Database;

class Product
{
    public ?int $id = null;
    public string $sku;
    public string $name;
    public float $price;
    public string $product_type;
    public ?array $prop_value;
    public ?string $prop_unit;
    public ?int $prop_id = null;

    public function load($data)
    {
        $this->id = $data['id'] ?? null;
        $this->name = $data['name'];
        $this->sku = $data['sku'];
        $this->price = $data['price'];
        $this->product_type = $data['product_type'];
        $this->prop_value = $data['prop_value']?? null;
        $this->prop_unit= $data['prop_unit'];
        $this->prop_id= $data['prop_id'] ?? null;
    }

    public function save()
    {
        $errors = [];
        if (!$this->name) {
            $errors[] = 'Please specify product name';
        }

        if (!$this->sku) {
            $errors[] = 'Please specify product SKU';
        }

        if (!$this->price) {
            $errors[] = 'Please specify product price';
        }

        if (!$this->prop_value) {
            $errors[] = 'Please specify product value of specified product type';
        }


        if (empty($errors)) {
            $db = Database::$db;
            $db->insertProduct($this);
        }
        return $errors;
    }
}