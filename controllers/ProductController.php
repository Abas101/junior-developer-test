<?php


namespace app\controllers;


use app\models\Product;
use app\Router;

class ProductController
{
    //Runs the db function to get products.
    public function index(Router $router)
    {
        $products = $router->db->getProducts();
        $router->render('index', ['products' => $products]);
    }


    //Controller to insert the form data into the db
    public function create(Router $router)
    {
        error_reporting(~E_ALL); //Turns off some notices that appear on the create page.
        $productData = [
            'sku' => '',
            'name' => '',
            'price' => '',
            'product_type' => '',
            'prop_value' => '',
            'prop_unit' => '',
        ];

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $productData['sku'] = $_POST['sku'];
            $productData['name'] = $_POST['name'];
            $productData['price'] =(float) $_POST['price'];
            $productData['product_type'] = $_POST['product_type'];
            $productData['prop_value'] = $_POST['prop_value'];
            $productData['prop_unit'] = $_POST['prop_unit'];
            //Creating a new product object
            $product = new Product();
            $product->load($productData);
            $errors = $product->save();
            if (empty($errors)) {
                header('Location: /products');
                exit;

            }
        }

        $router->render('create', [
            'product' => $productData,
            'errors' => $errors
        ]);
    }
    //Calls the deletion function from the db class
    public function delete(Router $router)
    {
        if(isset($_POST['product_id'])) {
            $router->db->massDelete();
        }else {
            $message = "You didnt select anything";
            echo "<script>alert('$message');</script>";
        }
        header("Location: /products");
    }

}