<?php


namespace app;
use app\models\Product;
use PDO;

class Database
{
    public ?PDO $pdo = null;
    public static ?Database $db = null;
    public function __construct()
    {   //Singleton class that connects to the db whenever any of the functions are called.
        try {
            $this->pdo = new PDO('mysql:host=localhost;port=3306;dbname=products', 'root', '');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$db = $this;
        }catch(\PDOException $e) {
            echo "Oops, Something went wrong, see the error: " . $e->getMessage();
        }
    }
        //Fetches for the products from the database
    public function getProducts()
    {

        $statement = $this->pdo->prepare('SELECT products.id, products.sku, products.name, products.price, products.product_type, products.prop_id, props.value, props.unit FROM products INNER JOIN props ON products.prop_id = props.id');
        $statement->execute();
        return $statement ->fetchAll(PDO::FETCH_ASSOC);
    }
        //Inserts products from the create.php form and binds the values into the query.
    public function insertProduct(Product $product) {
        $statement = $this->pdo->prepare("INSERT INTO props (value, unit) VALUES (:prop_value, :prop_unit)");
        $statement->bindValue(':prop_value', implode($product->prop_value));
        $statement->bindValue(':prop_unit', $product->prop_unit);
        $statement->execute();
    //Prepared statements should be able to avert any sql injection attempts
        $prop_id = $this->pdo->lastInsertId();
        $statement = $this->pdo->prepare("INSERT INTO products (sku, name, price, product_type, prop_id) VALUES (:sku, :name, :price, :product_type, :prop_id)");
        $statement->bindValue(':sku', $product->sku);
        $statement->bindValue(':name', $product->name);
        $statement->bindValue(':price', $product->price);
        $statement->bindValue(':product_type', $product->product_type);
        $statement->bindValue(':prop_id', $prop_id);
        $statement->execute();
    }
    //Gets an array of ids from the deletion form on the home page, converts them into one comma-seperated string and deletes the products via the query below.
    public function massDelete()
    {
        $products_to_delete = implode("','", $_POST['product_id']);
        $statement  = $this->pdo->prepare("DELETE FROM products WHERE id IN ('$products_to_delete')");
        var_dump($products_to_delete);
        $statement->execute();
    }
}