<?php require_once 'layouts\_header.php';?>
<?php if (!empty($errors)): //Outputs any error from the error array in the Product model?>
        <div class="alert alert-danger">
            <?php foreach ($errors as $error): ?>
                <div><?php echo $error ?></div>
            <?php endforeach; ?>
        </div>
<?php endif; ?>
<form method="post" enctype="multipart/form-data">
    <div class="form-group row">
        <label  class="col-sm-2 col-form-label">SKU</label>
        <div class="col-sm-10">
            <input type="text" name="sku" class="form-control" >
        </div>
    </div>
    <div class="form-group row">
        <label  class="col-sm-2 col-form-label">Product Name</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control" >
        </div>
    </div>
    <div class="form-group row">
        <label  class="col-sm-2 col-form-label">Price</label>
        <div class="col-sm-10">
            <input type="number" step="0.01" name="price" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label for="inputState">Product Type</label>
        <select id="inputState" class="form-control" name="product_type"  >
            <option value="Book">Book</option>
            <option value = "DVD">DVD-disc</option>
            <option value = "Furniture">Furniture</option>
        </select>
    </div>
    <script type="text/javascript">
            $(document).ready(function() { //Jquery function to make the form dynamic, making the chosen input visible
                $('#Book, #Furniture, #DVD').hide();
                $('#' + $('#inputState').val()).show()
                $("#inputState").change(function() {
                    $('#Book, #Furniture, #DVD').hide();
                    $(".propvalinput").val("");
                    $("#" + $(this).val()).show();
                    if($(this).val() === "Book") { // Checks the radio buttons whenever one of the types is selected (sorry yes its a conditional statement but I sadly couldn't find a more elegant way to do it)
                        $('#bookunit').prop("checked", true)
                    }else if($(this).val() === "DVD") {
                        $('#dvdunit').prop("checked", true)
                    }
                    else if($(this).val() === "Furniture") {
                        $('#dimension').prop("checked", true)
                    }

                });
            });
    </script>
    <div class="form-group row" id="Book">
            <label  for="bookweight" class="col-sm-2 col-form-label">Weight(KG)
            <input type="text" id="bookweight" step="0.01" name="prop_value[]" class="form-control propvalinput">
            <input type="radio" style="display: none" name="prop_unit" id="bookunit" value="KG" checked="checked">
            </label>
        </div>
    </div>

    <div class="form-group row" id="DVD">
            <label for="dvdsize" class="col-sm-2 col-form-label" name="prop_unit"> Size (MB)
                <input type="text" step="0.1" id="dvdsize" name="prop_value[]" class="form-control propvalinput">
                <input type="radio" style="display: none" name="prop_unit" id="dvdunit" value="MB">
            </label>
        </div>
    </div>
    <div class="form-group row" id="Furniture">
        <label  class="col-sm-2 col-form-label">Dimensions (HxWxL)
            <input type="text" name="prop_value[]" class="form-control propvalinput">
            <input type="radio" style="display: none" name="prop_unit" id="dimension" value="HxWxL">
        </label>
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Add Product</button>
</form>
<?php require_once 'layouts\_footer.php'?>