<?php require_once 'layouts\_header.php'; ?>
    <form method="post" action="/products/delete">
        <input type="submit" class="btn btn-danger container-sm" name="delete" value="Delete Selected" style="margin-bottom: 2em; width:20em" >

    <div class="d-flex flex-wrap" id="productrow">

<?php if (isset($products)) {
    // For loop to get all the products from the db
    foreach ($products as $i => $product) { ?>
            <div class="card" style="width: 60rem;">
                <div class="card-body">
                    <div class="form-check-inline">
                            <input type="checkbox" name="product_id[]" class="form-check-input" value="<?php echo $product["id"]?>">
                    </div>
                    <h5 class="card-title"><?php echo $product['name'] ?></h5>
                    <h6 class="card-subtitle mb-2"><?php echo $product['product_type'] ?></h6>
                    <?php if ($product['product_type'] == "Book") : ?>
                        <h6 class="card-subtitle mb-2 ">Weight: <?php echo $product['value'];
                            echo $product['unit'] ?></h6>
                    <?php elseif ($product['product_type'] == "DVD") : ?>
                        <h6 class="card-subtitle mb-2 ">Size : <?php echo $product['value'];
                            echo $product['unit'] ?></h6>
                    <?php elseif ($product['product_type'] == "Furniture") : ?>
                        <h6 class="card-subtitle mb-2 ">Dimensions : <?php echo $product['value'];
                            echo '(' . $product['unit'] . ')' ?></h6>
                    <?php endif; ?>
                    <h6 class="card-subtitle mb-2 ">Price: <?php echo $product['price'] ?>$</h6>
                    <h6 class="card-subtitle mb-2 ">SKU: <?php echo $product['sku'] ?></h6>
            </div>
        </div>
    <?php }
} else {
    echo "No Products Found";
} ?>
    </form>


<?php require_once 'layouts\_footer.php' ?>