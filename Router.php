<?php


namespace app;

//Custom Routing
class Router
{
    public array $getRoutes = [];
    public array $postRoutes = [];
    public Database $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    //Accepts urls from the getRoutes array
    public function get($url, $fn)
    {
        $this->getRoutes[$url] = $fn;

    }

    //Accepts urls from the postRoutes array
    public function post($url, $fn)
    {
        $this->postRoutes[$url] = $fn;

    }
    public function resolve()
    {
        $currentUrl = $_SERVER['PATH_INFO'] ?? '/'; //Gets current URL from the $_Server SuperGlobal
        $method = $_SERVER['REQUEST_METHOD']; //Gets the request method from the $_Server SuperGlobal

        switch ($method) { //Logic to get the respective function.
            case "GET":
                $fn = $this->getRoutes[$currentUrl] ?? null;
                break;
            case "POST":
                $fn = $this->postRoutes[$currentUrl] ?? null;
        }

        if ($fn) {
            call_user_func($fn, $this);
        }else {
            echo "404 Page not found";
        }
    }
    //Function to render a view
    public function render($view, $params = [])
    { //Creates an associative array from the accepted parameters
        foreach ($params as $key => $value) {
            $$key = $value;
        }
        include_once __DIR__."/views/$view.php";
    }
}
